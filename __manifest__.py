{
    "name": "CG SCOP - Agenda",
    "summary": "Adaptation agenda pour la CG Scop",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "license": "AGPL-3",
    "application": False,
    "depends": [
        'calendar',
        'cgscop_timesheet'
    ],
    "data": [
        "security/security_rules.xml",
        "views/calendar.xml",
        "wizard/calendar_event_transform.xml",
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'installable': True,
    'auto_install': False,
}
