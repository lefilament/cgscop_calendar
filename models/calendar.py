# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api
from odoo.exceptions import UserError


class CGScopCalendar(models.Model):
    _inherit = 'calendar.event'

    @api.model
    def _default_coop_id(self):
        if self.env.context.get('default_res_model') == 'res.partner':
            if self.env.context.get('default_res_id'):
                return self.env['res.partner'].browse(self.env.context.get('default_res_id'))
        return False

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    partner_ids = fields.Many2many(domain=[
        ('user_ids', '!=', False)])
    type = fields.Selection([
        ('outside', 'Extérieur'),
        ('ur', 'UR'),
        ('absent', 'Absence, Congés, Divers')],
        string="Type de Réunion")
    location = fields.Text()
    coop_id = fields.Many2one(
        comodel_name='res.partner',
        string='Contact',
        domain=[('is_company', '=', 'True')],
        default=_default_coop_id)
    project_id = fields.Many2one(
        comodel_name="project.project",
        string="Code Activité UR",
        domain=[('allow_timesheets', '=', True)])
    cgscop_timesheet_code_id = fields.Many2one(
        related='project_id.cgscop_timesheet_code_id',
        string='Code Activité National',
        store=True)
    ur_financial_system_id = fields.Many2one(
        comodel_name='ur.financial.system',
        string='Dispositif Financier')
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)
    ur_financial_system_nb = fields.Integer(
        string="Nb Dispositifs Financiers",
        compute="_compute_ur_financial_system_nb")

    # ------------------------------------------------------
    # Compute
    # ------------------------------------------------------
    @api.depends('ur_id')
    def _compute_ur_financial_system_nb(self):
        for event in self:
            financial_system = event.env['ur.financial.system'].search([
                ('ur_id', '=', event.ur_id.id)])
            event.ur_financial_system_nb = len(
                financial_system)

    # ------------------------------------------------------
    # Onchange
    # ------------------------------------------------------
    @api.onchange('project_id')
    def _onchange_project_id(self):
        if self.project_id.partner_id:
            self.coop_id = self.project_id.partner_id

    @api.onchange('coop_id')
    def _onchange_coop_id(self):
        address = ''
        if self.coop_id.street:
            address += self.coop_id.street + '\n'
        if self.coop_id.street2:
            address += self.coop_id.street2 + '\n'
        if self.coop_id.street3:
            address += self.coop_id.street3 + '\n'
        if self.coop_id.zip:
            address += self.coop_id.zip + ' '
        if self.coop_id.city:
            address += self.coop_id.city
        self.location = address

    # ------------------------------------------------------
    # Fonction boutons
    # ------------------------------------------------------
    @api.multi
    def create_timesheet(self):
        """ Crée une ligne de temps à partir de l'entrée d'agenda
        """
        for event in self:
            if not event.project_id.analytic_account_id:
                raise UserError("Le code activité UR doit être \
                                renseigné sur chaque entrée d'agenda")
            else:
                self.env['account.analytic.line'].create({
                    'user_id': self.env.user.id,
                    'project_id': event.project_id.id,
                    'account_id': event.project_id.analytic_account_id.id,
                    'ur_financial_system_id': event.ur_financial_system_id.id,
                    'date': event.start,
                    'name': event.name,
                    'company_id': self.env.user.company_id.id,
                    'unit_amount': event.duration if not event.allday else 8.0,
                    'partner_id': event.coop_id.id,
                })
                # event.is_transfered = True
