# -*- coding: utf-8 -*-
from odoo import models, api


class AccountInvoiceConfirm(models.TransientModel):
    """
    This wizard will transform the all the selected
    calendar entries to timesheet
    """

    _name = "calendar.event.transform"
    _description = "Confirm the selected invoices"

    @api.multi
    def events_confirm(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        events = self.env['calendar.event'].browse(active_ids)
        events.create_timesheet()
        return {'type': 'ir.actions.act_window_close'}
